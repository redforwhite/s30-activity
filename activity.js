db.fruits.count();



db.fruits.aggregate([
{$match: {onSale: true}},
{$count: "fruitsOnSale"}
]);

db.fruits.aggregate([
{ $match: { stock: { $gte: 20 } } },
{ $count: "fruitsGte20" }
])

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {
		_id: "$supplier_id",
		averagePrice: {$avg: "$price"}
		}
	}
]);

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {
		_id: "$supplier_id",
		maxPrice: {$max: "$price"}
		}
	}
]);

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {
		_id: "$supplier_id",
		minPrice: {$min: "$price"}
		}
	}
]);